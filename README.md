# Garfielpp example

This example uses Garfield++ to get macroscopic quantities of different gas mixtures.
The simulation runs on htcondor and the gas mixures are specified in the `htcondor.sub` file.

The electric field was chosen to be around the range of the working point of a 2 mm HPL RPC detector.

A bash script is used to load the proper ROOT and Garfield environment from the LCG repository. The script
is run from within Lxplus
