#! /bin/sh

source /cvmfs/sft.cern.ch/lcg/views/LCG_102swan/x86_64-centos7-gcc11-opt/setup.sh 
source /cvmfs/sft.cern.ch/lcg/views/LCG_102swan/x86_64-centos7-gcc11-opt/share/Garfield/setupGarfield.sh

python3 /eos/user/g/grigolet/code/garfield-simulations/simulation_htcondor.py $1
