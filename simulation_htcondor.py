#! /usr/bin/python3

import ROOT
import os, sys
import ctypes

path = os.getenv('GARFIELD_INSTALL')

if sys.platform == 'darwin':
  ROOT.gSystem.Load(path + '/lib64/libmagboltz.dylib')
  ROOT.gSystem.Load(path + '/lib64/libGarfield.dylib')
else:
  ROOT.gSystem.Load(path + '/lib64/libmagboltz.so')
  ROOT.gSystem.Load(path + '/lib64/libGarfield.so')

def process_gas(gas_mixture):
    gas = ROOT.Garfield.MediumMagboltz()
    print(f"Reading {gas_mixture}", flush=True) 
    gas.SetComposition(*gas_mixture)
    gas.SetTemperature(293.15)
    gas.SetPressure(760) 
    gap = 0.2
    vstart = 6000 / gap
    vstop = 10000 / gap
    gas.SetFieldGrid(vstart, vstop, 40, False)
    ncoll = 11 # in multiple of 10^7
    gas.GenerateGasTable(ncoll)
    filename = str('_'.join(str(i) for i in gas_mixture))
    gas.WriteGasFile(f"/eos/project/g/ghg-studies/misc/Simulations/{filename}.gas")
    print(f"Gas file written: {filename}")


if __name__ == '__main__':
    argument = sys.argv[-1]
    composition = [*(item for item in argument.split('_'))]
    composition = [float(item) if index % 2 == 1 else str(item) for index, item in enumerate(composition)]
    process_gas(composition)
